class Transforms
  @@R = /(\s|-|_|\.|`)/

  def self.to_url( string )
    return Addressable::URI.parse( string )
  end

  def self.to_specific( string )
    string.gsub(@@R, '').downcase
  end
end
