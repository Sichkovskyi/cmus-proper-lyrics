require 'open-uri'
require 'nokogiri'
require 'addressable/uri'
require 'user_agent_randomizer'

require 'cmus-proper-lyrics/transforms'

$issuesURL = 'https://gitlab.com/Sichkovskyi/cmus-proper-lyrics/-/issues'

class Lyrics
  @@currentSong = ''
  @@retries = 0

  def self.update
    out = `cmus-remote -Q`

    if out.empty?
      puts `clear`
      puts "CMUS is likely not running. Waiting for some music..."

      puts "\n\n\n\nIf you know for sure that your CMUS is running, please open an issue at #{$issuesURL}" if @@retries > 10;

      @@retries += 1

      return
    end

    artist = out.match(/artist\s(.*)\n/)
    title = out.match(/title\s(.*)\n/)

    if artist.nil? or title.nil?
      puts `clear`
      puts "Script needs an artist and a title to find lyrics.\n\n"
      puts "\n\n\n\nIf you know for sure that there are an artist and title available in tags for current song, please open an issue at #{$issuesURL}" if @@retries > 10

      @@retries += 1

      return
    end

    @@retries = 0

    unless (@@currentSong == "#{ artist[1] } - #{ title[1] }") then
      user_agent = UserAgentRandomizer::UserAgent.fetch(type: "desktop_browser")

      urlized_artist = Transforms.to_specific artist[1]
      urlized_title = Transforms.to_specific title[1]

      @@currentSong = "#{ artist[1] } - #{ title[1] }"
      url = Transforms.to_url "https://www.azlyrics.com/lyrics/#{ urlized_artist }/#{ urlized_title }.html"

      puts `clear`
      puts "#{ @@currentSong }\n\n"
      puts "#{ url }\n\n"

      begin
        html = URI.open(
          url.display_uri.to_s,
          'User-Agent' => user_agent.string
        )

        doc = Nokogiri::HTML5(html)

        lyrics = doc.xpath("/html/body/div[2]/div/div[2]/div[5]")

        puts lyrics[0].text
      rescue OpenURI::HTTPError => e
        puts "\n\nNOT FOUND"
      end

    end

  end

  def self.start
    loop do
      self.update
      sleep 5
    end
  end
end
