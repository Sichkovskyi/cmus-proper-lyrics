Gem::Specification.new do |s|
  s.name        = 'cmus-proper-lyrics'
  s.version     = '0.0.1'
  s.summary     = "What the title says"
  s.description = "Small thingy that fetches lyrics for the song tha's currently playing in CMUS. That's it."
  s.authors     = ["Yurii Sichkovskyi"]
  s.email       = 'yurii@synapps.agency'
  s.files       = [
    "lib/cmus-proper-lyrics.rb",
    "lib/cmus-proper-lyrics/transforms.rb"
  ]
  s.executables << 'lyrics'
  s.homepage    =
    'https://gitlab.com/Sichkovskyi/cmus-proper-lyrics'
  s.license     = 'GNU General Public License v2.0'
end
