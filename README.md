# Fetch lyrics for current CMUS Song

![Screenshot](./screenshot.png)

A very simple Ruby script that fetches lyrics from MusicXMatch, refreshes once in 5 seconds and clears terminal. That's it.

I did this for fun and I use this myself :D 
